from fastapi import FastAPI, Header
from pydantic.main import BaseModel
from typing import Optional

import uvicorn
import json
import time
import cv2
import os
import dlib
import logging

from google.cloud import storage
from fastapi.middleware.cors import CORSMiddleware


class FromFrontendRequest(BaseModel):
    group_id: int
    project_id: int
    image_name: str
    object: list
    bounding: dict

app = FastAPI()
origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def explicit(env):
    storage_client = storage.Client.from_service_account_json(
        'mykey.json')
    if env == 'dev':
        bucket = storage_client.get_bucket("cw_platform_dev")
    else:
        bucket = storage_client.get_bucket("cw_platform")
    return bucket

def download_blob(source_blob_name, destination_file_name, env):
    """Downloads a blob from the bucket."""
    bucket = explicit(env)
    blob = bucket.blob(source_blob_name)
    blob.download_to_filename(destination_file_name)

def get_file_from_gcs(group_id, project_id, image_name, env):
    bucket_dir = str(group_id) + '/' + str(project_id) + '_content/_content_data/' + image_name
    file_dir = './static/dup_dataset/'
    os.makedirs(file_dir, exist_ok=True)
    filename_tmp = './static/dup_dataset/' + image_name
    download_blob(bucket_dir, filename_tmp, env)
    filename = file_dir + image_name
    return filename

@app.post(path='/result/')
async def result(request: FromFrontendRequest, referer: Optional[str] = Header(None, convert_underscores=False)):
    start_time = time.time()

    group_id = request.group_id
    project_id = request.project_id
    image_name = request.image_name
    object = request.object
    bounding = request.bounding

    print("Header(Referer)", referer)
    if referer.startswith('https://devco.'): # 개발 환경인 경우
        env = 'dev'
    elif referer.startswith('https://client'):
        env = 'worker'
    else: # etc.
        env = 'dev'

    filename = get_file_from_gcs(group_id, project_id, image_name, env)

    try:
        image = cv2.imread(filename, cv2.IMREAD_COLOR)
    except:
        logging.error("[+] Open image with cv2 Failed.")
    
    # image_content = requests.get(filename).content
    # image_nparray = np.asarray(bytearray(image_content), dtype=np.uint8)
    # img = cv2.imdecode(image_nparray, cv2.IMREAD_COLOR)

    img_size_x, img_size_y = image.shape[1], image.shape[0]
    print("[+] Image Size: ", img_size_x, img_size_y)

    # Face Landmark 68 Points

    ## face detector와 landmark predictor 정의
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

    rects = detector(image)
    print("{} faces are detected.".format(len(rects)))
    all_faces = []
    for i, rect in enumerate(rects):
        l = rect.left()
        t = rect.top()
        b = rect.bottom()
        r = rect.right()
        point_list = []
        shape = predictor(image, rect)
        for j in range(68):
            x, y = shape.part(j).x, shape.part(j).y
            print(x, y)
            point = {
                "x": x,
                "y": y
            }
            point_list.append(point)
            cv2.circle(image, (x, y), 3, (0, 0, 255), -1)
            cv2.putText(image, str(j + 1), (x, y), cv2.FONT_HERSHEY_PLAIN, 1, (255, 0, 0), 1)
        # cv2.rectangle(img, (l, t), (r, b), (0, 255, 0), 2)
        # cv2.imwrite('annotated_image/result.png', image)
        output = {
            "annotation": "DOTS",
            "obj_name": "face",
            "points": point_list
        }
        all_faces.append(output)
    end_time = time.time()
    print("Processed : ", (end_time - start_time) )

    return json.loads(json.dumps(all_faces))


"""
{
    "annotation": "POLYGONS",
    "obj_name": "oval",
    "points": [{'x': ...,'y':...,'z':... }]

}
"""

if __name__ == '__main__':
    uvicorn.run("gateway:app",
                host="0.0.0.0",
                port=9444,
                reload=True,
                ssl_keyfile="./mlgpu.duckdns.org/privkey.pem", 
                ssl_certfile="./mlgpu.duckdns.org/cert.pem"
                )

