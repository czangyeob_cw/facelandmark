FROM nvidia/cuda:11.0-cudnn8-devel-ubuntu18.04

RUN apt-get update && apt-get install -y cmake git python3 python3-pip libgl1-mesa-glx \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

RUN git clone https://github.com/davisking/dlib.git
WORKDIR /dlib
RUN mkdir build; cd build; cmake .. -DDLIB_USE_CUDA=1 -DUSE_AVX_INSTRUCTIONS=1; cmake --build .

RUN python3 setup.py -v install

RUN pip3 install --upgrade setuptools pip
RUN pip3 install scikit-build
RUN pip3 install fastapi
RUN pip3 install uvicorn
RUN pip3 install opencv-python
RUN pip3 install google-cloud-storage
RUN pip3 install aiofiles

COPY . /

WORKDIR /app
CMD /usr/bin/python3 gateway.py

